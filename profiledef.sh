#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="Vela_OS"
iso_label="Vela_OS_$(date +%Y%m)"
#iso_publisher="Arch Linux <https://archlinux.org>"
iso_application="Custom Arch Linux Live Media"
iso_version="$(date +%d.%m.%Y)"
install_dir="arch"
buildmodes=('iso')
bootmodes=('uefi-ia32.systemd-boot.esp' 'uefi-x64.systemd-boot.esp'
           'uefi-ia32.systemd-boot.eltorito' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'zstd' '-Xcompression-level' '15' '-b' '1M')
file_permissions=(
  ["/etc/os-release"]="0:0:400"
  ["/etc/shadow"]="0:0:400"
  ["/etc/gshadow"]="0:0:400"
  ["/etc/sudoers"]="0:0:400"
  ["/root"]="0:0:750"
  ["/usr/local/bin/"]="0:0:755"
)
